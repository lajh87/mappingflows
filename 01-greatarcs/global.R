library(shiny)
library(maptools)
library(magrittr)
library(leaflet)

data(wrld_simpl)
m <- quantile(wrld_simpl@data$POP2005, 0.75)
spdf <- wrld_simpl[wrld_simpl@data$POP2005>m,]

nodes <- spdf@data %>% dplyr::select("NAME", "LON", "LAT")
l <- nrow(nodes)
links <- expand.grid(
  from = nodes$NAME[2:l], 
  to = nodes$NAME[1:(l-1)]
)
links$id <- 1:nrow(links)
links$value <- runif(n = nrow(links),1,50)

links <- links %>% 
  dplyr::left_join(
    nodes %>% 
      dplyr::rename(from = NAME, from_lon = LON, from_lat = LAT)
  ) %>%
  dplyr::left_join(
    nodes %>%
      dplyr::rename(to = NAME, to_lon = LON, to_lat = LAT)
  )

p1 <- as.matrix(links[,c(5,6)])
p2 <- as.matrix(links[,c(7,8)])

links2 <- geosphere::gcIntermediate(
  p1, p2,
  breakAtDateLine = TRUE,
  n = 100,
  addStartEnd = TRUE,
  sp = T)

links2 <- sp::SpatialLinesDataFrame(
  sl = links2, 
  data = data.frame(id = 1:nrow(links))
  )

links2@data <- links2@data %>%
  dplyr::left_join(links %>% dplyr::select(id, from, to, value )) %>%
  dplyr::mutate(width = scales::rescale(value, c(1,8))) %>%
  dplyr::mutate(opacity = scales::rescale(value, c(0.05,0.4)))
