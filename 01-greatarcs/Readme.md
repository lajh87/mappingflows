This shiny application demonstrates leaflets events. Click on a circle and see
the flows change. The width and opacity of the flow line indicate the size 
of the value between countries.